# Useful Design Notes

[How to Make Almost Anything: Mechanical Design](http://academy.cba.mit.edu/classes/mechanical_design/index.html)
[How to Make Almost Anything: Machine Design](http://academy.cba.mit.edu/classes/machine_design/index.html)
[How to Make Something that Makes Almost Anything](https://fab.cba.mit.edu/classes/865.21/index.html)

## MechE

[slocum: FUNdaMENTALS](http://pergatory.mit.edu/resources/fundamentals.html)  
[mechanical design principles](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/principles/)  
[transmissions](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/transmissions/)  
[kinematics](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/kinematics/)  
[materials](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/materials/)  
[common mechanical design patterns](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/approaches/)

## EE (power electronics):

[common switching architectures](https://fab.cba.mit.edu/classes/865.21/topics/power_electronics/architectures/)  
[commutating motors](https://fab.cba.mit.edu/classes/865.21/topics/power_electronics/commutation/)

## Metrology:

[accuracy vs. precision](https://fab.cba.mit.edu/classes/865.21/topics/metrology/01_concepts.html)

## MechE (examples)

- clank
- corexy (of dave)
- beehive axes
- clank-mudstack-extruder design pattern
- ... add some more