## Rec 1

- bring each example axis 
- bring the blot 
- bring one kit 
  - image of each thing / notes on hardware 
- past years' work ... 
- other resources 

... should be ... 

- what you'll have to do,
  - mechanism, 
  - actuation, 
  - automation, 
- what this normally involves, architecturally ? 
  - it's the mess, but don't scare them ? 
- what are some fun ideas for machines we can build ?
  - scale can be small (remember you're going to bring the thing to class)
  - it can be simple (remember you have only a week!)
- what are some good resources ?
  - we have prepared some, 
    - incld. machine class content, of which... meche, 
    - incld. our example axes, etc, 
  - others have, 
    - slocum, ... 
    - open builds ? 
- what you should do:
  - elect a Machine Dictator (you can deploy democratic process internally, we want to have one point person)
  - have them contact Jake (WhatsApp) (617 230 0791) (jake.read@cba.mit.edu)
  - and Quentin 
  - convene a meeting to decide:
    - what to build, who to do what, when to work together, etc, 
  - invite us to the first meeting, 
  - we will bring you your kit of stuff to that first meeting 


## Rec 2

... a walkthrough of some nuts-and-bolts demos, 

- the xylophone, 
  - requires a representation of the plan (the sequencer) 
  - and an interface to it, 
  - requires some planning (when can we hit... what?) 
  - requires some actuation, 
  - and mechanical design... 
- the scara-arm
  - ... requires some kinematics, etc, 
- the resources are, broadly,
  - modules (of which any arduino project can become one such) 
  - mechanical bits and bops 
- adjoints
  - the extrusions use slide-in nuts, 
  - we commonly print 4.4mm hole to self-thread an M5, 
    - more beef wants hex inserts 