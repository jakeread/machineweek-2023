# Machine Week 2023

```
everything's connected but nothing's working !
```

**Toiler Paper Plotter:** [Arch Section Repo](https://gitlab.cba.mit.edu/classes/863.23/Architecture/machine)  
**Damatictron:** [CBA Section Repo](https://gitlab.cba.mit.edu/classes/863.23/CBA/machine) / [CBA Section Website](https://fab.cba.mit.edu/classes/863.23/CBA/machine/index.html)   
**Gershenforcer:** [EECS Section Repo](https://gitlab.cba.mit.edu/classes/863.23/EECS/machine) / [EECS Website](https://fab.cba.mit.edu/classes/863.23/EECS/machine/index.html)  
**Photo Bleach:** [Harvard Section Repo](https://gitlab.cba.mit.edu/classes/863.23/Harvard/machine) / [Harvard Website](https://fab.cba.mit.edu/classes/863.23/Harvard/machine/index.html)   

# 1. Your (Group) Task

Each section will design and build a "machine" - this means anything that includes **mechanism, actuation, automation and application**, that just means:

### **Mechanism:** mechanical degrees-of-freedom (DOF)

Stuff shuld move around.

### **Actuation:** computer-controllable motion of the above DOFs

The motion should be computer controlled.

### **Automation / Application** software-coordinated computer-control of the above, and i.e. sequences therein

The motion should result in some desired outcome... maybe make something, draw something, change something, assemble something, sort something, cast a spell, etc.

--- 

# 2. Tools, Tools, Tools !

> [Ilan](https://web.mit.edu/imoyer/www/index.html) (inventor of [CoreXY](http://www.corexy.com/)) would urge us to think more carefully about "tools" - what does it mean to build a computer-controlled system that acts like a tool, interacting with the world but being mediated by some designer's intent... it's a [foamcore world](https://web.mit.edu/imoyer/www/portfolio/foamcore/index.html).

### Shaper Origin [[YouTube](https://www.youtube.com/watch?v=QxjE5WOAGi4)]

![origin](img/shaper_origin.webp)

### Turn-By-Wire [[UIST](https://dl.acm.org/doi/abs/10.1145/3332165.3347918)]

![tbw](img/turn_by_wire.jpg)

---

# 3. Examples / Ideas

The machines you design and build **don't need to be complicated** - try to de-risk ideas early, and do stuff that "works right away" rather than complex projects that require hundreds of details to come together all at once in order to work. This can be fun; have fun.

### [Light Painting](img/maxl-hello-world.jpg)
### [Label Maker](https://www.youtube.com/watch?v=Y_rrbo6_42U&t=73s)
### [Wire Cutter](https://youtu.be/7E6iHRjvH_k?t=20)
### [Music Machines](https://youtu.be/dhKYxDzyXqI?t=5) aka [Clangers / Bangers](https://ekswhyzee.com/2023/07/21/clangers-and-bangers.html) 
### [String / Floating Machines](https://youtu.be/dZLtPFJEQi0?t=104)
### [Architecture-Scale / Space Transforming](https://fab.cba.mit.edu/classes/865.21/people/gilsunshine/systems.html)
### [Claw Machine](https://youtu.be/zR3BLM_TAmg?t=117)
### [Robot Chainsaw Machine](https://youtu.be/ix68oRfI5Gw?t=1160)
### [Robot Basketball Hoop](https://youtu.be/myO8fxhDRW0?t=598)

--- 

# 4. Strategies

- teamworks! 
  - have a project manager 
  - divide into sub-teams, but talk often 
    - electronics / software
    - mechanism
    - documentation 
    - cad/cam ? 
- prototype in cardboard, mechanisms can move 'by hand'
- commit to your vision (no backtracking)
- spiral development !
- document all the time, it should be one person's job !
  - one page per team, 
  - note individual contributions on your page 

---

# 5. Examples from this Kit

### Framing

T gusset [[CAD](CAD/parts/gusset_T.f3z)][[stl](CAD/parts/gusset_T.stl)]

<img src=img/gusset_T.png width=50%><br>

Elbow gusset [[CAD](CAD/parts/gusset_elbow.f3z)][[stl](CAD/parts/gusset_elbow.stl)]

<img src=img/gusset_elbow.png width=50%><br>

### Belt Axis [[CAD](CAD/belt_axis/belt_axis.f3z)]

<img src=img/belt_axis.png width=50%><br>

Kit [[stl](CAD/belt_axis/belt_axis_kit.stl)]

<img src=img/belt_axis_kit.png width=50%><br>

### Leadscrew Axis

Assembly [[CAD](CAD/leadscrew_axis/leadscrew_axis.f3z)]

<img src=img/leadscrew_axis.png width=50%><br>

Kit [[stl](CAD/leadscrew_axis/leadscrew_axis_kit.stl)]

<img src=img/leadscrew_axis_kit.png width=50%><br>

### Rotary Axis

Assembly [[CAD](CAD/rotary_axis/rotary_axis.f3z)] [[notes](https://ekswhyzee.com/2019/04/09/gt2-belt-rotary-cad.html)]

<img src=img/rotary_axis.png width=50%><br>

Kit [[stl](CAD/rotary_axis/rotary_axis_kit.stl)]

<img src=img/rotary_axis_kit.png width=50%><br>

## The Blot / Drawing Machine [[notes](https://blot.hackclub.dev/)]

![blot](img/blot-clear-bg.png)

## The Xylophone 

![xylo](img/xylophone_teaser.jpg)

## Scara Arm [[YouTube](https://www.youtube.com/watch?v=1QHJksTrk8s&t=38s)]

TODO

---

# 6. [The Kit](KIT.md)

## [Modular Things !](https://github.com/modular-things/modular-things/)

### [... Circuits](https://modular-things.github.io/modular-things/things/) 

---

# 7. What to Do Now

- elect a contact-person (or dictator)
- have them contact us (Jake and Quentin) (contact info will be in a gitlab issue) 
- convene a kickoff meeting, schedule it with us!
- kits, demos, and more to come... 

--- 

# 8. [Extended Resources](LINKS.md)