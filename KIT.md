# The Kit

| Part | QTY | Ordered ? | Arrived ? | Kitted ? |
| --- | --- | --- | --- | --- |
| **Hardware** | --- | --- | --- | --- |
| FHCS M5x10 | x | x | x | x |
| FHCS M5x20 | x | x | x | x |
| FHCS M5x30 | x | x | x | x |
| FHCS M5x40 | x | x | x | x |
| SHCS M5x10 | x | x | x | x |
| SHCS M5x20 | x | x | x | x |
| Nylock M5  | x | x | x | x |
| Washer M5 | x | x | x | x |
| FHCS M3x8 | x | x | x | x |
| SHCS M3x8 | x | x | x | x |
| SHCS M3x16 | x | x | x | x |
| Nylock M3  | x | x | x | x |
| Washer M3 | x | x | x | x |
| [Aluminum Spacer](https://www.mcmaster.com/94669A042/) | 10 | x | x | x |
| [Eccentric Spacers](https://www.amazon.com/Micro-Traders-Eccentric-Aluminium-Extrusion/dp/B09CYK9P43/) | 10 | x | x | no |
| **Extrusion Situations** | --- | --- | --- | --- |
| [V-Groove Extrusion 2020](https://www.amazon.com/BC-Labs-Slot-Aluminum-Extrusion/dp/B092HD1353/) | 2x 1000mm | x | x | x |
| HFS5-2060 | 3x 2000mm | x | x | x |
| Extrusion Corner Brackets | 20 | x | x | x |
| [Extrusion Post-Install T-Nuts](https://www.amazon.com/Qjaiune-Aluminum-Extrusion-Printer-Profile/dp/B09H5B55ZG/) | x | x | x | x |
| **USB** | --- | --- | --- | --- |
| [8-Port Powered USB Hub](https://www.amazon.com/Sokiwi-Aluminum-Expender-Splitter-Individual/dp/B099YS5DGX/?th=1) | 1 | x | x | x |
| USB A to C Cables | 7 | x | x | x |
| [USB C to C 240W Cables](https://www.amazon.com/LISEN-Charger-Certified-Charging-MacBook/dp/B0CL26CTYG/) | 1 | x | x | x |
| [USB PD Charger](https://www.amazon.com/Charger-Charging-Station-Foldable-Portable/dp/B0C6K2PJJ8/) | 1 | x | x | x |
| [USB PD Decoys](https://www.amazon.com/Type-C-Trigger-Polling-Detector-Notebook/dp/B0BCW2LQP8) | 2 | x | x | x |
| **Motors** | --- | --- | --- | --- |
| [NEMA17 Motors](https://www.amazon.com/STEPPERONLINE-Stepper-Bipolar-4-Lead-Printer/dp/B00QEYADRQ/) | 5 | x | x | x |
| [Standard Size Servos](https://www.amazon.com/Youleke-Torque-Digital-Servo%EF%BC%8CWaterproof-Horn%EF%BC%88270%C2%B0%EF%BC%89/dp/B08739MGPL/) | 2 | x | x | x |
| [Mini Size Servos](https://www.amazon.com/Miuzei-MG90S-Servo-Helicopter-Arduino/dp/B0BWJ26PX2/) | 2 | x | x | x |
| **Transmission** | --- | --- | --- | --- |
| Kevlar 8800K43 | 50ft | x | x | no |
| [GT2 Pulleys](https://www.amazon.com/Printing-Zeelo-Fiberglass-Rostock-Printers/dp/B08SMFM3Z6/?th=1) | 4 | x | x | x |
| [GT2 Belt Open](https://www.amazon.com/Printing-Zeelo-Fiberglass-Rostock-Printers/dp/B08SMFM3Z6/?th=1) | - | x | x | x |
| GT2 Belt Closed 280mm | 2 | x | x | x |
| [6808 Bearings](https://www.amazon.com/XIKE-6808-2RS-Bearings-40x52x7mm-Pre-Lubricated/dp/B09D2VY3BK/?th=1) | 2 | x | x | x |
| [625 Bearings](https://www.amazon.com/uxcell-625-2RS-Bearing-5x16x5mm-Bearings/dp/B07TML6YP4/?th=1) | 10 | x | x | x |
| [625F Bearings](https://www.amazon.com/%EF%BC%BB12-Pack%EF%BC%BD625-2RS-Ball-Bearings-Miniature/dp/B0BRQP2QG7/) | 10 | x | x | x |
| [Standard Roller Wheels](https://www.amazon.com/Official-Creality-3D-Printer-Plastic/dp/B09QPG7XGR/) | 20 | x | x | x |
| [Small Roller Wheels](https://www.amazon.com/3Dman-Plastic-Pulley-Passive-Bearing/dp/B07V4GHLL8/) | 20 | x | x | x |
| **Misc** | --- | --- | --- | --- |
| [Limit Switches](https://www.amazon.com/JANDECCN-Switch-Straight-Action-V-153-1C25/dp/B0BDDCYTPP/) | 5 | x | x | x |
