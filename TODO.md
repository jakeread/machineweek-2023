# Machine Week TODO 

## Demo Desires

- finish the xylophone, in-modular-things, and as a standalone ?
	- do it for the deploy / wall sitter 
- scara plotter ! 
- want a way to bottle demos
	- cad / cam / circuits, each as standalone mono-repo ? 
	- web UI is for sketching, deploy is different... 
- a plotter ? (servo and axes) 
- a lil robot arm, 3dof 
- accelerometer-to-stage ? 
- etc-a-sketch the plotter
- a walking robot ? it'd be a good demo for the graphs thing also, anyways... sequencing, etc... 
